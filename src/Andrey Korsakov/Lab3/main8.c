/*
�������� ���������, ������� ����������� � ������������ ������, ���������
�� ���������� ���� � ����� ����� n, � ����� ������� n - �� ����� ������ ��
�����. � ������ ������������� n ��������� ��������� �� ������
*/

#include <stdio.h>

char str[80] = { 0 };
int i = 0;
int n = 0;
int flag = 0;
int count = 0;
int inWord = 0;

int main()
{
	while (flag != 1)
	{
		printf("Enter a string of several words: \n");	//������� ������ �� ���������� ����
		fflush(stdin);
		fgets(str, 80, stdin);
		printf("Enter the word number: \n");			//������� ����� �����
		fflush(stdin);
		scanf("%d", &n);

		while (str[i] != '\n')
		{
			if (str[i] == ' ' && inWord == 0)
				i++;
			else if (str[i] != ' ' && inWord == 0)
			{
				inWord = 1;
				count++;
			}
			else if (str[i] != ' ' && inWord == 1)
			{
				if (count == n)
				{
					flag = 1;
					while (str[i] != ' ' && str[i] != '\n')
					{
						printf("%c", str[i]);
						i++;
					}
				}
				else
					i++;
			}
			else
				inWord = 0;
		}

		if (flag != 1)
			printf("The word number is not valid, please try again: \n");	//����� ����� ������ �� �����, ��������� �������
		else
			printf(" - %d word\n", n);

		i = 0;
		n = 0;
		count = 0;
		inWord = 0;
	}

	return 0;
}