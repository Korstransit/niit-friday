/*
�������� ���������, ������� ��������� ������������� ������ ������� N,
� ����� ������� ����� ��������� ����� ����������� � ������������ ����������.
*/

#include <stdio.h>
#include <time.h>

char str[80] = { 0 };
int i = 0;
int N = 0;
int sum = 0;
int max;
int min;
int imax = 0;
int imin = 0;
int ran = 0;


int main()
{
	printf("Enter a number between 2 and 80:\n");
	scanf("%d", &N);

	while (N < 2 || N > 80)
	{
		printf("Sorry, repeat again:\n");		//��������� �����
		fflush(stdin);
		scanf("%d", &N);
	}
	
	srand(time(0));

	while (i < N)
	{
		ran = 1 + rand() % 256;
		str[i] = ran;
		printf("%d, ", str[i]);
		i++;	
	}
	
	max = str[0];
	min = str[0];

	for (i = 0; i < N; i++)
	{
		if (str[i] > max)
		{
			max = str[i];
			imax = i;
		}
		else if (str[i] < min)
		{
			min = str[i];
			imin = i;
		}
	}
	printf("\nMin: %d, Max: %d", min, max);

	if (imin < imax)
	{
		while (imin < imax - 1)
		{
			imin++;
			sum += str[imin];
		}
	}
	else
	{
		while (imax < imin - 1)
		{
			imax++;
			sum += str[imax];
		}
	}
	printf("\nSum:%d\n", sum);

	return 0;
}