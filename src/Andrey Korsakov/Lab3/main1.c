/*
�������� ���������, �������������� ���������� ���� �� �������� ������������� ������
*/
#include <stdio.h>
#include <string.h>

int main()
{
	char str[80] = { 0 };
	int i = 0;
	int count = 0; 
	int inWord = 0;	
	printf ("Enter an arbitrary string: \n");
	fgets(str, 80, stdin);

	while (str[i] != '\n')
	{
		if (str[i] != ' ' && inWord == 0)
		{
			inWord = 1;
			count++;
		}
		else if ((str[i] == ' ' || str[i] == '\n') && inWord == 1)
			inWord = 0;

		i++;
	}
	printf("Number of words: %d\n", count);

	return 0;
}