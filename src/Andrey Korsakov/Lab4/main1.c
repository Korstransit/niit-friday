/*
�������� ���������, ������� ��������� ������������ ������ ��������� 
����� � ����������, � ����� ��������� �� � ������� ����������� ����� ������.
���������:
������ �������� �� ��������� ������ ������ � ������������ � ���������
���������� ������.������������ ���������� �������� � ���������� �������
���������� �� char.����� ��������� ����� ��������� ��������� �������� �
������� � ������� ������ � ������������ � ���������������� �����������.
*/


#include <stdio.h>
#include <string.h>
#include <stdlib.h>

#define N 10
#define M 80

char str[N][M];
char *p[N];
int i = 0, count = 0;

int main()
{
	printf("Enter several arbitrary lines: \n");	//������� ��������� ������������ �����

	while (i < N && *fgets(str[i], M, stdin) != '\n')
	{
		p[i] = str[i++];
	}
	count = i;

	i = 1;
	while (i < count)
	{
		if (i == 0 || strlen(p[i - 1]) <= strlen(p[i]))
			i++;
		else
		{
			char *temp = p[i];
			p[i] = p[i - 1];
			p[i - 1] = temp;
			i--;
		}
	}

	for (i = 0; i < count; i++)
	{
		printf("%s", p[i]);
	}

	return 0;
}